const argv = require('./config/yargs').argv

//Cuando se tiene llaves despúes de "const" y "let" es una destructuración  
const { crearArchivo, listarTabla } = require('./multiplicar/multiplicar');

let comando = argv._[0];

switch (comando) {

    case 'listar':
        listarTabla(argv.base, argv.limite);
        break;

    case 'crear':
        crearArchivo(argv.base, argv.limite)
            .then(archivo => console.log(`Archivo creado: ${ archivo}`))
            .catch(e => console.log(e));
        break;

    default:
        console.log('Comando no reconocido');
}

//let base = '8';
//console.log(process.argv);

//Se utiliza para obtener datos enviados por consola
//let argv2 = process.argv;
//console.log(argv.base);
//let parametro = argv[2];
//let base = parametro.split('=')[1]

// crearArchivo(base)
//     .then(archivo => console.log(`Archivo creado: ${ archivo}`))
//     .catch(e => console.log(e));